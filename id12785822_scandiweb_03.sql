-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 23, 2021 at 09:33 PM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `id12785822_scandiweb_03`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories_00`
--

CREATE TABLE `categories_00` (
  `cat_id` int(11) NOT NULL,
  `cat_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cat_attribute` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories_00`
--

INSERT INTO `categories_00` (`cat_id`, `cat_name`, `cat_attribute`) VALUES
(1, 'DVD disc', 'Please, provide the DVD disc size@JVC@Size@MB@ @Size (MB)'),
(3, 'Furniture', 'Please, provide the furniture dimensions@GGWP@Dimensions@ @x@Height (CM)@Width (CM)@Length (CM)'),
(5, 'Book', 'Please, provide the book weight@TR@Weight@KG@ @Weight (KG)');

-- --------------------------------------------------------

--
-- Table structure for table `categories_01`
--

CREATE TABLE `categories_01` (
  `cat_id` int(11) NOT NULL,
  `cat_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `cat_attribute` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories_01`
--

INSERT INTO `categories_01` (`cat_id`, `cat_name`, `cat_attribute`) VALUES
(4, 'Book', 'Please, provide the book weight@TR@Weight@KG@ @Weight (KG)'),
(4, 'DVD-disc', 'Please, provide the DVD-disc size@JVC@Size@MB@ @Size (MB)'),
(4, 'Furniture', 'Please, provide the furniture dimensions@GGWP@Dimensions@ @x@Height (CM)@Width (CM)@Length (CM)');

-- --------------------------------------------------------

--
-- Table structure for table `categories_02`
--

CREATE TABLE `categories_02` (
  `cat_id` int(11) DEFAULT NULL,
  `cat_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `cat_attribute` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products_00`
--

CREATE TABLE `products_00` (
  `pro_id` int(11) NOT NULL,
  `cat_name` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cat_id` int(10) DEFAULT NULL,
  `pro_SKU` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pro_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pro_price` double(6,2) UNSIGNED ZEROFILL DEFAULT NULL,
  `pro_attribute` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products_00`
--

INSERT INTO `products_00` (`pro_id`, `cat_name`, `cat_id`, `pro_SKU`, `pro_name`, `pro_price`, `pro_attribute`) VALUES
(5, 'DVD disc@Size@MB@ ', 1, 'JVC200123', 'Acme DISC', 001.00, '700@'),
(7, 'Furniture@Dimensions@ @x', 3, 'TR120555', 'Chair', 040.00, '24@45@15@'),
(24, 'Furniture@Dimensions@ @x', 4, 'JVC200123', 'autoload.php', 001.00, '2@7@700@'),
(28, 'Furniture@Dimensions@ @x', 4, 'Emilio tankai', 'Juozas Krusna', 012.00, '3@2@1@');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories_00`
--
ALTER TABLE `categories_00`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `categories_01`
--
ALTER TABLE `categories_01`
  ADD PRIMARY KEY (`cat_name`) USING BTREE;

--
-- Indexes for table `categories_02`
--
ALTER TABLE `categories_02`
  ADD PRIMARY KEY (`cat_name`);

--
-- Indexes for table `products_00`
--
ALTER TABLE `products_00`
  ADD PRIMARY KEY (`pro_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories_00`
--
ALTER TABLE `categories_00`
  MODIFY `cat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `products_00`
--
ALTER TABLE `products_00`
  MODIFY `pro_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
