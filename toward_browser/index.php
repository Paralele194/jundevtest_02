<?php
	$page = isset($_GET['page']) ? $_GET['page'] : 1;
	$records_per_page = 15;
	$from_record_num = ($records_per_page * $page) - $records_per_page;
	include_once '../toward_server/database.php';
	include_once '../toward_server/product.php';
	include_once '../toward_server/category_01.php';
	include '../toward_server/category_insert_01.php';
	include '../toward_server/update.php';
	$database = new Database();
	$db = $database->getConnection();
	$product = new Product($db);
	$category = new Category($db);
	$insert = new Insert($db);
	$stmt = $product->readAll($from_record_num, $records_per_page);
	$page_title = "Product List";
	$button_1_title = "ADD";
	$button_2_title = "MASS DELETE";
	$button_1_class = "btn btn-default pull-right";
	$button_2_class = "btn btn-success btn-lg float-right";
	$button_1_type = "button";
	$button_2_type = "button";
	$button_1_href = "create_product.php";
	$button_2_href = "index.php";
	$form_id = "fn1()";
	$button_1_all = "<form  method='POST' action='../toward_server/scan_file.php'  >
        <button><a    type={$button_1_type} >{$button_1_title}</a></button> 
    </form>";
	$button_2_all = "<button><a href={$button_2_href} type={$button_2_type} name='btn_delete' id='btn_delete' class={$button_2_class}
>{$button_2_title}</a></button>";
	$page_footer = "Scandiweb Test assignment";
	echo"<script src='https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js'>";echo"</script>";
	echo"<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css' />";
	echo"<script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js'>";echo"</script>";
	echo"<meta charset=utf-8><link rel = 'stylesheet' type = 'text/css' href = '../keepUpAppearances/_includes.css'>";
?>
<!DOCTYPE html>
<html>
    <head>
    </head>
    <body>
        <div class="header">
            <?php
	            include_once'layout_header.php';
            ?>
        </div>
        <div class="body_x">
            <?php
            	echo"<div class='container'>";
	        	while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){  
		        	extract($row);
			        echo"<div class='item'>";
    				echo"<div class='item_input'><input type='checkbox' name='id[]' value= $pro_id/><br></div>";
    				echo"<div class='item_text'><label for='id[]'>$pro_SKU</label><br></div>";
	    			echo"<div class='item_text'><label for='id[]'>$pro_name</label><br></div>";
		    		echo"<div class='item_text'><label for='id[]'>$pro_price</label><label> </label><label>$</label><br></div>";
    				$common_string_01 = '';
    				$sub_string_arr_00 = (explode("@",$cat_name));
    				$sub_string_arr_01 = (explode("@",$pro_attribute));
    				$attr_dim = count($sub_string_arr_01);
    				for ($i =0; $i < $attr_dim-1;  $i++){
    					if ($i < $attr_dim-2) {
    						$gap_fill = $sub_string_arr_00[3];
    					}else{
    						$gap_fill = '';
    					}
    					$common_string_01 .=$sub_string_arr_01[$i].$gap_fill;
	    			}
    				echo"<div class='item_text'><label for='id[]'>$sub_string_arr_00[1]</label><label>: </label><label for='id[]'>$common_string_01</label><label> </label><label for='id[]'>$sub_string_arr_00[2]</label><br></div>";
	        		echo"</div>";
                }
            	echo"</div>";
            	echo"<script>
	            	$(document).ready(function(){
    			        $('#btn_delete').click(function(){
	    			        if(confirm('Delete this?')){
		    			        var id = [];
			    		        $(':checkbox:checked').each(function(i){
				    		        id[i] = $(this).val();
				            	});
				            	if(id.length === 0) {
						            alert('Select checkbox');
    					        }else{
	    					        $.ajax({
		    					    url:'../toward_server/delete.php',
			    				    method:'POST',
				    			    data:{id:id},
					    	        });
					            }
    			        	}else{
	    				        return false;
		    		        }
                        });
                    });
	            </script>";
            	$page_url = "index.php?";
	            $total_rows = $product->countAll();
            ?>
        </div>
    <div class="footer">
        <?php
        	include_once "layout_footer.php";
        ?>
    </div>
    </body>
</html>
