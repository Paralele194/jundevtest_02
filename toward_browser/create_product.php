<?php
	include_once '../toward_server/database.php';
	include_once '../toward_server/product.php';
	include '../toward_server/category_01.php';	
	$database = new Database();
	$db = $database->getConnection();
	$product = new Product($db);
	$category = new Category($db);
	$page_title = "Product Add";
	$button_1_title = "Save";
	$button_2_title = "Cancel";
	$button_1_class = "";
	$button_2_class = "";
	$button_1_type = "submit";
	$button_2_type = "button";
	$button_1_href = "../toward_browser/index.php";
	$button_2_href = "../toward_browser/index.php";
	$button_1_form = "../toward_browser/create_product";
	$button_1_all = "<button type={$button_1_type} form='create_product' href={$button_1_href} >{$button_1_title}</button>";
	$button_2_all = "<button><a href={$button_2_href} type={$button_2_type} name='btn_delete' id='btn_delete' class={$button_2_class}>{$button_2_title}</a></button>";

	$page_footer = "Scandiweb Test assignment";
	if($_POST){
		$product->cat_name = $_POST['cat_name'];
		$product->cat_id = $_POST['cat_id'];
		$product->pro_SKU = $_POST['pro_SKU'];
		$product->pro_name = $_POST['pro_name'];
		$product->pro_price = $_POST['pro_price'];
		$product->pro_attribute = $_POST['pro_attribute'];
		$product->create();
	}
?> 
<!DOCTYPE html>
<html>
<head>
	<script src='https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
	<script src="https://ajax.googleapis.com/ajax/libs/mootools/1.6.0/mootools.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script>
	<meta charset=utf-8><link rel = 'stylesheet' type = 'text/css' href = '../keepUpAppearances/_includes.css'>
</head>
<body>
    <div class = "header">
        <?php
            include_once "layout_header.php";
        ?>
    </div>
        <div class="Body">
            
	<form id="create_product" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post" >

			<div class='body_input'><label>SKU</label><input type='text' name='pro_SKU' ><br></div>	  
			<div class='body_input'><label>Name</label><input type='text' name='pro_name' ><br></div>	  
			<div class='body_input'><label>Price ($)</label><input type='text' name='pro_price' ><br></div>	  
			<?php
				$stmt = $category->read();
				echo "<div class='body_input'><select  name='category_id' id='product_type' onchange='myFunction()'>";
				echo "<option>Type Switcher</option>";
				while ($row_category = $stmt->fetch(PDO::FETCH_ASSOC)){
					extract($row_category);
					$common_string = $cat_id.'@'.$cat_name.'@'.$cat_attribute;
					echo "<option value='{$common_string}'>{$cat_name}</option>";
				}
				echo "</select></div>";
			?>
			<script>
				var container = $(document.createElement('div'));
				var container_cat_id = $(document.createElement('div'));
				var container_cat_name = $(document.createElement('div'));
				var container_politeness = $(document.createElement('div'));
                var divValue = $(document.createElement('div'));
				function myFunction() {
					var product_type_xxx = document.getElementById("product_type").value;
					var product_type_xxx = 1;
					$(container)
					.empty()
					.remove();     
					i = 0; 
					var xxx_02 =(document.getElementById("product_type").value);
					var xxx_01 =parseInt(document.getElementById("product_type").value);
					var xxx_03 =xxx_02.split("@");
					var product_type_xxx = (xxx_03.length)-7;
					for (var i = 0; i < product_type_xxx ; i++) {
						var xxx_00 =3+i;
						var xxx_01 =100;
						$(container).append('<div class="body_attribute"><label>'+xxx_03[7+i]+ ':</label><input type="text" class="input" name="pro_attribute" size="30" maxlength="100" onchange="GetTextValue()" id=tb' + i + ' ' +  'value="" >');
					}
					$(container_politeness).append('<p>'+xxx_03[2]+ ':</p><br>');
					$('#main_politeness').after(container_politeness); 
					$(container_cat_id).append('<input type="hidden" name="cat_id" value="'+xxx_03[0]+'">');
					$(container_cat_name).append('<input type="hidden" name="cat_name" value="'+xxx_03[1]+'@'+xxx_03[4]+'@'+xxx_03[5]+'@'+xxx_03[6]+'">');
					$('#main_cat_id').after(container_cat_id); 
					$('#main_cat_name').after(container_cat_name); 
					$('#main').after(container); 
				}
				var divValue, values = '';
				function GetTextValue() {
					$(divValue) 
					.empty() 
					.remove(); 
					values = '';
					$('.input').each(function() {
						values += this.value + '@'
					});
					$(divValue).append('<input type="hidden" name="pro_attribute" size="30" maxlength="100" value="'+values+'">');
					$('#main_cumulative').after(divValue); 
				}
			</script>
			<div class="body_attribute_add">	
				<div  class="container_xxx">
					<div id="main">
					</div>
					<div id="main_cumulative">
					</div>
					<div id="main_politeness">
				</div>
	    		<div  class="container_cat_id">
		    		<div id="main_cat_id">
			    	</div>
    			</div>
	    		<div  class="container_cat_name">
		    		<div id="main_cat_name">
			    	</div>
		    	</div>
	    	</div>
<!-- 
		</div>
<!-- -->
	</form>
</div>
<div class="footer">
<?php
	include_once "layout_footer.php";
?>
</div>
</body>
</html>

