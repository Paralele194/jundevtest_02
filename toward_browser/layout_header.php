<!DOCTYPE html>
<html lang="en">
<head>  
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php echo $page_title; ?></title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
</head>
<body>
	<div>  

		<?php
			echo "<div class = 'header_text'>
				<p>{$page_title}<p>
            </div>";
			echo "<div class = 'header_button'>
				{$button_1_all}
				{$button_2_all}
			</div>";
		?>
	</div>
</body>
</html>
