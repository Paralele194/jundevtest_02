<?php
	class Product{
		private $conn;
		private $table_name = "products_00";
		public $pro_id;
		public $cat_name;
		public $cat_id;
		public $pro_SKU;
		public $pro_name;
		public $pro_price;
		public $pro_attribute;
		public function __construct($db){
			$this->conn = $db;
		}
		function create(){

			$query = "INSERT INTO
				" . $this->table_name . "
				SET
				cat_name=:cat_name, cat_id=:cat_id, pro_SKU=:pro_SKU, pro_name=:pro_name, pro_price=:pro_price, pro_attribute=:pro_attribute";
			$stmt = $this->conn->prepare($query);
			$this->cat_name=htmlspecialchars(strip_tags($this->cat_name));
			$this->cat_id=htmlspecialchars(strip_tags($this->cat_id));
			$this->pro_SKU=htmlspecialchars(strip_tags($this->pro_SKU));
			$this->pro_name=htmlspecialchars(strip_tags($this->pro_name));
			$this->pro_price=htmlspecialchars(strip_tags($this->pro_price));
			$this->pro_attribute=htmlspecialchars(strip_tags($this->pro_attribute));
			$stmt->bindParam(":cat_name", $this->cat_name);
			$stmt->bindParam(":cat_id", $this->cat_id);
			$stmt->bindParam(":pro_SKU", $this->pro_SKU);
			$stmt->bindParam(":pro_name", $this->pro_name);
			$stmt->bindParam(":pro_price", $this->pro_price);  
			$stmt->bindParam(":pro_attribute", $this->pro_attribute);  
			if($stmt->execute()){
				return true;
			}else{
				return false;
			}
		}
		function readAll($from_record_num, $records_per_page){
			$query = "SELECT
				pro_id, cat_name, cat_id, pro_SKU, pro_name, pro_price, pro_attribute
				FROM
				" . $this->table_name . "
				ORDER BY
				pro_name ASC
				LIMIT
				{$from_record_num}, {$records_per_page}";
			$stmt = $this->conn->prepare( $query );
			$stmt->execute();
			return $stmt;
		}
		public function countAll(){  
			$query = "SELECT id FROM " . $this->table_name . "";
			$stmt = $this->conn->prepare( $query );
			$stmt->execute();
			$num = $stmt->rowCount();
			return $num;
		}
	}
?>
