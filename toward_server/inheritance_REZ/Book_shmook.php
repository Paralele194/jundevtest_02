<?php
    require_once 'Product.php';
    class Book_shmook extends Product{
        public function __construct(){
            $Name = 'Book_shmook selected';
            $this->_Name = $Name;
            $this->_Type = 'Book_shmook';
        }
        public function display(){
            echo "<p>Book_shmook: $this->_Name </p>";
        }
        public function _w_construct(){

            $this->_Name_[0] = 'Please, provide the book_shmook weight';
            $this->_Name_[1] = 'TR';            
            $this->_Name_[2] = 'Weight';            
            $this->_Name_[3] = 'KG';            
            $this->_Name_[4] = ' ';            
            $this->_Name_[5] = 'Weight (KG)';            
            $this->_Type = 'Book_shmook';
            return ($this->_Name_);
        }
        public function _c_construct(){

            $this->_Name_[0] = 'Book_shmook';            
            $this->_Type = 'Book_shmook';
            return ($this->_Name_);
        }
    }