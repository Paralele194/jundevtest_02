<?php
    require_once 'Product.php';
    class DVD_disc extends Product{
        public function __construct(){
            $Name = 'DVD-disc selected';
            $this->_Name = $Name;
            $this->_Type = 'DVD-disc';
        }
        public function display(){
            echo "<p>DVD-disc: $this->_Name </p>";
        }
        public function _w_construct(){
            $this->_Name_[0] = 'Please, provide the DVD-disc size';
            $this->_Name_[1] = 'JVC';
            $this->_Name_[2] = 'Size';
            $this->_Name_[3] = 'MB';
            $this->_Name_[4] = ' ';
            $this->_Name_[5] = 'Size (MB)'; 
            $this->_Type = 'DVD-disc';
            return($this->_Name_);
        }

        public function _c_construct(){

            $this->_Name_[0] = 'DVD-disc';
            $this->_Type = 'DVD-disc';
            return($this->_Name_);
        }
    }