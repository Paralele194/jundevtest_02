<?php
    abstract class Product{
        // properties defined here
        protected $_SKU;
        protected $_Name;
        protected $_Price;
        protected $_Type;
        protected $_Type_value;
//        private $_errors[];
        // methods defined here

        public function getName(){
            return $this->_Name;
        }
        abstract public function display();
        abstract public function _w_construct();
        abstract public function _c_construct();
    }